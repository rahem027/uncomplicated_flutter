library uncomplicated_flutter;

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:uncomplicated/uncomplicated.dart';
/// also export the package so anyone importing uncomplicated_flutter
/// also gets uncomplicated
export 'package:uncomplicated/uncomplicated.dart';

typedef ExplicitBuilderCallback<T> = Widget Function(
    BuildContext context, T value);

class ExplicitBuilder<T> extends StatefulWidget {
  final ExplicitState<T> state;
  final ExplicitBuilderCallback<T> builder;

  const ExplicitBuilder({Key? key, required this.state, required this.builder})
      : super(key: key);

  @override
  State<ExplicitBuilder<T>> createState() => _ExplicitBuilderState<T>();
}

class _ExplicitBuilderState<T> extends State<ExplicitBuilder<T>> {
  late final StreamSubscription<T> _subscription;

  @override
  void initState() {
    super.initState();
    _subscription = widget.state.listen((_) => setState(() {}));
  }

  @override
  Widget build(BuildContext context) =>
      widget.builder(context, widget.state.value);

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }
}

typedef ComputedDataCallback<T> = Widget Function(BuildContext context, T data);
typedef ComputedWaitingCallback = Widget Function(BuildContext context);
typedef ComputedErrorCallback = Widget Function(
    BuildContext context, dynamic error, StackTrace? stackTrace);

class ComputedBuilder<T> extends StatefulWidget {
  final ComputedState<T> state;
  final ComputedDataCallback<T> data;
  final ComputedErrorCallback error;
  final ComputedWaitingCallback waiting;

  const ComputedBuilder({
    Key? key,
    required this.state,
    required this.waiting,
    required this.error,
    required this.data,
  }) : super(key: key);

  @override
  State<ComputedBuilder<T>> createState() => _ComputedBuilderState<T>();
}

class _ComputedBuilderState<T> extends State<ComputedBuilder<T>> {
  late final ComputedSubscription _subscription;

  @override
  void initState() {
    super.initState();
    _subscription = widget.state.listen((_) => setState(() {}));
  }

  @override
  Widget build(BuildContext context) => widget.state.value.match(
    data: (T data) => widget.data(context, data),
    error: (error, stacktrace) => widget.error(context, error, stacktrace),
    waiting: () => widget.waiting(context),
  );

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }
}
