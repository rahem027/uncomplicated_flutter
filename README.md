<!--
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages).

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages).
-->

# uncomplicated_flutter
Flutter specific builders for uncomplicated package. Very lightweight 
wrapper on top of `uncomplicated_package`. Contains two widgets:

1. ExplicitBuilder that builds takes an `Explicitstate` and returns
a widget
    ```dart
    final _state = ExplicitState(0);
    
    ExplicitBuilder(
      state: _state,
      builder: (BuildContext context, int counter) {
        return Text(
          '$counter',
          style: Theme.of(context).textTheme.headlineMedium,
        );
      },
    );
    ```
2. ComputedBuilder that takes an `ComputedState` and the 3 callbacks
    ```dart
    
    final _state = ComputedState(
      builder: () => Future.delayed(const Duration(seconds: 1), () => 0)
    );
    
    ComputedBuilder(
      state: _state,
      waiting: (BuildContext context) => Center(CircularProgressIndicator()),
      error: (BuildContext context, dynamic error, StackTrace? stacktrace) {
        return Column(
          children: [
            Text(error?.toString ?? 'Something went wrong'),
            Text(stacktrace?.toString ?? ''),
          ];
        );
   
      },
      data: (BuildContext context, counter) {
        return Text(
          '$counter',
          style: Theme.of(context).textTheme.headlineMedium,
        );
      },
    );
    
    ```

## Features

1. Easy to use
2. Only 1 dependency: `uncomplicated`
3. Entire library is some 300 SLOC (uncomplicated) + 100 SLOC (uncomplicated flutter). 
Less code means less bugs

## Getting started

Check out documentation of [uncomplicated](https://pub.dev/packages/uncomplicated)
